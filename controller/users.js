const User = require("../models/User");
const Product = require('../models/Product')
const bcrypt = require("bcrypt")
const dotenv = require ("dotenv");
let salt = parseInt(process.env.SALT);
const auth = require('../auth.js')
const mongoose = require("mongoose");
const db = mongoose.connection;

dotenv.config();

// User Registration
module.exports.createUser = (requestBody) => {
	let newUser = new User({
		email: requestBody.email,
		password: bcrypt.hashSync(requestBody.password,salt)
	});
	return newUser.save().then((user,err) => {
		if(user){
			return user;
		} else {
			return 'Failed to Register account';
		};
	});
};

// Admin Registration
module.exports.createAdmin = (requestBody) => {
	let newUser = new User({
		email: requestBody.email,
		password: bcrypt.hashSync(requestBody.password,salt),
		isAdmin: true
	});
	return newUser.save().then((user,err) => {
		if(user){
			return user;
		} else {
			return 'Failed to Register account';
		};
	});
};

// User authentication
module.exports.loginUser = (data) => {
	return User.findOne({email: data.email}).then(result => {
		if(result == null){
			return false;
		} else {
			const isPasswordCorrect = bcrypt.compareSync(data.password, result.password)
			if (isPasswordCorrect){
				return{accessToken: auth.createAccessToken(result.toObject())}
			}else{
				return false;
			}
		}
	})
}

// Non-admin User Checkout
module.exports.checkout = async (req, res) => {
		if(req.user.isAdmin) {
				return res.send({ message: "Action Forbidden"})
		}

		let isUserUpdated = await User.findById(req.user.id).then(user => {
				let newPurchase = {products:[{
				productName: req.body.productName,
				quantity: req.body.quantity
				}],
				totalAmount: req.body.quantity * req.body.price
			}
			user.orders.push(newPurchase);

			return user.save().then(user => true).catch(err => err.message)
			
			if (isUserUpdated !== true){
				return res.send({message: isUserUpdated})
			}
		});

		let isProductUpdated = await Product.findById(req.body.productId).then(product => {
			console.log (req.user.id)
			let orderId = {orderId:req.user.id}
			product.orders.push(orderId);

			return product.save().then(product => true).catch(err => err.message)
			if(isProductUpdated !== true){return res.send ({message:isProductUpdated})}
		})

		if (isUserUpdated && isProductUpdated){return res.send({message:"Successfully checked-out order!"})}
}

// Set User as Admin (Admin only)
module.exports.adminUser = (userId) => {
	let updateActiveField = {
		isAdmin: true
	};
	return User.findByIdAndUpdate(userId, updateActiveField).then((user, error) => {
		if(error){
			return false;
		} else {
			return `${user.email} has been given Admin rights`;
		}
	}).catch(error => error)
}

// Revoke Admin Access (Admin only)
module.exports.notAdminUser = (userId) => {
	let updateActiveField = {
		isAdmin: false
	};
	return User.findByIdAndUpdate(userId, updateActiveField).then((user, error) => {
		if(error){
			return false;
		} else {
			return `${user.email}'s Admin rights has been revoked`;
		}
	}).catch(error => error)
}

// Get All Users (Admin only)
module.exports.getAllUsers = () => {
	return User.find({}).then(result => {
		return result
	})
}

// Retrieve authenticated user's orders
module.exports.getOrders = (req,res) => {
	User.findById(req.user.id)
	.then(result => res.send(result.orders))
	.catch(err => res.send(err))
}

// Get All Orders (Admin only)
module.exports.getAllOrders = () => {
	return User.find({},{email:0,password:0,isAdmin:0}).then(result => {
		return result
	})
}

module.exports.getProfile = (data) => {
	return User.findById(data).then(result => {
		result.password = '';
		return result;
	})
}
const Product = require('../models/Product')

// Create Product
module.exports.addProduct = (reqBody) => {
	let newProduct = new Product ({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	});

	return newProduct.save().then((product,error) =>{
		if(product){
			return product;
		} else {
			return 'Failed to create product';
		};
	}).catch(error => error)
}

// Retrieve all active products
module.exports.getAllActive = () => {
	return Product.find({isActive:true}).then(result => {
		return result;
	}).catch(error => error)
}

// Retrieve single product
module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams).then(result => {
		return result;
	}).catch(error => error);
}

// Update product information (Admin only)
module.exports.updateProduct = (productId,data) => {
	let updatedProduct = {
		name: data.name,
		description: data.description,
		price: data.price
	}
	return Product.findByIdAndUpdate(productId,updatedProduct).then((product,error) => {
		if(error){
			return false;
		} else {
			return true;
		}
	}).catch(error => error)
}

module.exports.getAllProducts = () => {
	return Product.find({}).then(result => {
		return result;
	})
}

// Archive Product (Admin only)
module.exports.archiveProduct = (productId) => {
	let updateActiveField = {
		isActive: false
	};
	return Product.findByIdAndUpdate(productId, updateActiveField).then((product, error) => {
		if(error){
			return false;
		} else {
			return `${product.name} has been archived`;
		}
	}).catch(error => error)
}

// Activate Product (Admin only)
module.exports.activateProduct = (productId) => {
	let updateActiveField = {
		isActive: true
	};
	return Product.findByIdAndUpdate(productId, updateActiveField).then((product, error) => {
		if(error){
			return false;
		} else {
			return `${product.name} has been reactivated`;
		}
	}).catch(error => error)
}
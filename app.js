const express = require("express");
const mongoose = require("mongoose");
const cors = require('cors');
const dotenv = require ("dotenv");
const userRoute = require("./routes/users");
const productRoute = require("./routes/products")
const app = express();

dotenv.config();
let link = process.env.LINK;

const port = process.env.PORT;

mongoose.connect(link,{

    useNewUrlParser:true,
    useUnifiedTopology:true

});

const db = mongoose.connection;

db.on('error', console.error.bind(console,"Connection Error"));
db.once('open',() => console.log("Connected to Database"));

app.use(express.json());
app.use(cors());
app.use(express.urlencoded({extended:true}));
app.use('/users',userRoute);
app.use('/products',productRoute);

app.get('/',(req,res) => {res.send('Welcome to Shop the Tuck Up!')})
app.listen(port,() => console.log(`Server running at port ${port}`));
const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
	name: {type: String, required: [true, 'Product Name is Required']},
	description: {type: String, required: [true, 'Description is Required']},
	price: {type: Number, required: [true, 'Product Price is Required']},
	isActive: {type: Boolean, default: true},
	createdOn: {type: Date, default: new Date()},
	orders: [{orderId: {type: String, required: [true, 'Order ID is Required']}}]
})

module.exports = mongoose.model("Product", productSchema);
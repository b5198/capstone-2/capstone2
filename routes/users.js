const express = require("express");
const router = express.Router();
const userController = require ("../controller/users")
const auth = require('../auth')
const { verify, verifyAdmin } = auth;

// User Registration
router.post("/register", (req,res) => {
	userController.createUser(req.body).then(resultFromController => res.send(resultFromController))
})

// Admin Registration (Admin Only)
router.post("/regadmin", verify, verifyAdmin, (req,res) => {
	userController.createAdmin(req.body).then(resultFromController => res.send(resultFromController))
})

// User Authentication(login)
router.post('/login', (req, res) => {
	userController.loginUser(req.body).then(result => res.send(result));
})

//Set user as Admin (Admin only)
router.put('/:userId/admin', verify, verifyAdmin, (req, res) => {
	userController.adminUser(req.params.userId).then(result => res.send(result));
})

//Set user as Admin (Admin only)
router.put('/:userId/revokeadmin', verify, verifyAdmin, (req, res) => {
	userController.notAdminUser(req.params.userId).then(result => res.send(result));
})

// Get all users (Admin only)
router.get("/", verify, verifyAdmin, (req,res) => {
	userController.getAllUsers().then(resultFromController => res.send(resultFromController))
})

// GET the user's details
router.get("/details", auth.verify, (req,res) => {
	userController.getProfile(req.user.id).then(result => res.send(result));
})

// Non-admin User Checkout
router.post('/checkout', auth.verify, userController.checkout);

// Get authenticated user's orders
router.get('/orders', auth.verify, userController.getOrders);

// Get all orders (Admin only)
router.get("/allorders", verify, verifyAdmin, (req,res) => {
	userController.getAllOrders().then(resultFromController => res.send(resultFromController))
})
module.exports = router;
const express = require('express');
const route = express.Router();
const ProductController = require('../controller/products')
const auth = require('../auth');
const { verify, verifyAdmin } = auth;

// Create Product (Admin only)
route.post('/create', verify, verifyAdmin, (req, res) => {
	ProductController.addProduct(req.body).then(result => res.send(result))
})

// Retrieve all products
route.get('/all'/*,verify, verifyAdmin*/, (req,res) => {
	ProductController.getAllProducts().then(result => res.send(result));
})

// Retrieve all ACTIVE products
route.get('/active', (req,res) => {
	ProductController.getAllActive().then(result => res.send(result));
})

// Retrieving a SPECIFIC product
route.get('/:productId',(req,res) => {
	ProductController.getProduct(req.params.productId).then(result => res.send(result))
})

//Route for UPDATING a product (Admin only)
route.put('/:productId', verify, verifyAdmin, (req, res) => {
	ProductController.updateProduct(req.params.productId, req.body).then(result => res.send(result))
})

//Archiving a product (Admin only)
route.put('/:productId/archive', verify, verifyAdmin, (req, res) => {
	ProductController.archiveProduct(req.params.productId).then(result => res.send(result));
})

//Activating a product (Admin only)
route.put('/:productId/activate', verify, verifyAdmin, (req, res) => {
	ProductController.activateProduct(req.params.productId).then(result => res.send(result));
})

module.exports = route;